#! /bin/sh

echo This script is still experimental..


if [[ "$(whoami)" == root ]]; then

  echo This will take a while...
  # Yes, it's dirty. But hey, we don't have to rebuild the image
  kill -USR1 1 

else
  echo "You need to run this as root!"
  echo ">   docker exec --user root obfs4-bridge update.sh"
fi
