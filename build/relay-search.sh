#!/bin/sh

TOR_LOG=${TOR_DIR}/log

fingerprint=$(grep "Your Tor bridge's hashed identity key fingerprint is" "$TOR_LOG" | \
	    sed "s/.*\([0-9A-F]\{40\}\)'$/\1/" | \
	        tail -1)

echo "https://metrics.torproject.org/rs.html#details/${fingerprint}" 
