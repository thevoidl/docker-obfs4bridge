#!/bin/ash
# Using this dirty trick for updating
trap updateTor 10 #USR1
trap stopTor INT
TORRC=$TOR_DIR/torrc

main(){
  if [[ "$TOR_MAIL" = "ididnotchangetheconfig" ]] \
     || [[ -z "$TOR_MAIL" ]] \
     || [[ -z "$MY_IP4" && -z "$MY_IP6"  ]] ; then
    echo "PLEASE EDIT THE .env FILE PROPERLY!"
    exit 1;
    #TODO: evaluate the pid file exists as expected
  elif [[ ! -e /var/run/tor/tor.pid ]] ; then
    echo " ====== Generating torrc file with OR_PORT=${TOR_OR_PORT}, OBFS4_PORT=${TOR_OBFS4_PORT}, and EMAIL=${TOR_MAIL} ======"
    echo
    writeTORRC > $TORRC
    chown tor $TORRC
  else
    echo "Did you mean to upgrade? Then run ´docker exec <containername> update.sh´"
  fi

  chown -R tor ${TOR_DIR}/keys

  # Starting health loop
  while [[ true ]] ; do
    # note that we need the head command here. Sometimes this command gets its own pid
    TOR_PID=$(ps | grep 'tor -f' | awk '{print $1}' | head -c2)
  
    [[ -z "$TOR_PID" ]] && startTor

#  echo torpid $TOR_PID
#  echo torrc $TORRC
#  echo nick ${TOR_NICK}

    sleep 20
  done
}
##########################
#      END of main
##########################


startTor(){
  echo "Starting tor..."
  su -s /bin/sh tor -c "tor -f $TORRC" &
  fg
}

stopTor(){
  kill -INT $TOR_PID 
  exit 0;
}

updateTor(){
  [[ -z "$TOR_PID" ]] || stopTor

  echo ====== Updating tor ======
  echo ' > Getting alpine updates...'
  apk -U upgrade --no-cache

  echo ' > Updating obfs4...'
  cd /obfs4
  git pull || \
   [[ rm ${OBFS4_DIR}/obfs4proxy && cp ./obfs4proxy/obfs4proxy ${OBFS4_DIR} && chown tor ${OBFS4_DIR}/obfs4proxy  ]]
}

############################################################################
# You can add configuration options here and rebuild the image if you want #
# More information here: https://community.torproject.org/relay/           #
writeTORRC()
{
  echo "RunAsDaemon 0"
  echo "PidFile /var/run/tor/tor.pid"
  echo
  echo "## We don´t need an open SOCKS port and don't want to be public as bridge"
  echo "SocksPort 0"
  echo "BridgeRelay 1"
  echo
  echo "## Logging to the file as well as to stdout for docker"
  echo "Log notice file ${TOR_DIR}/log"
  echo "Log notice stdout"
  echo
  echo "## Where to store the stuff"
  echo "ServerTransportPlugin obfs4 exec ${OBFS4_DIR}/obfs4proxy"
  echo "DataDirectory ${TOR_DIR}"
  echo
  echo "## Listen on this port for obfs4 connections to picking a port automatically"
  echo "ORPort ${TOR_OR_PORT}"
  echo "ServerTransportListenAddr obfs4 0.0.0.0:${TOR_OBFS4_PORT}"
  echo "ExtORPort auto"
  echo
  if [[ ! -z "${TOR_MAX_ACCOUNTING}" ]]; then
    echo "## Accounting information. You can limit per month"
    echo "RelayBandwidthRate ${TOR_BANDWITH_RATE}"
    echo "RelayBandwidthBurst ${TOR_BANDWITH_BURST}"
    echo "AccountingMax ${TOR_MAX_ACCOUNTING}"
    echo "AccountingStart month 1 00:00"
  fi
  echo
  echo "## Your contact info"
  echo "Nickname ${TOR_NICK}"
  echo "ContactInfo ${TOR_MAIL}"
}

# Executing here
main "$@"
