#!/bin/ash
echo
echo == Testing the installation ==
echo Tor_dir:   $TOR_DIR
echo obfs4_dir: $OBFS4_DIR
echo 
echo nickname:  $TOR_NICK
echo mail:      $TOR_MAIL
echo orport:    $TOR_OR_PORT
echo obfsport:  $TOR_OBFS4_PORT
echo obfsport from the outside: Unavailable in the container 
echo
echo Bridgeline:
get-bridge-line.sh
echo
