#! /bin/bash

main(){
echo ======= Building your docker-compose file =======
echo This will override any existing docker-compose.yml
echo
echo "You can use .env file or interactively define each environment variable."
read -p "Use .env to beautify docker-compose file? [Y/n] " -n 1 -r
echo

set -a
. .env

if [[ $REPLY =~ ^[Nn]$ ]] ; then
  setNewVariables
else
  cat .env
  echo
fi

substitute

if [[ $EXPOSE_OBFS_AT -le 1024 ]] ; then
  LOWPORT="$EXPOSE_OBFS_AT"
elif [[ $TOR_OR_PORT -le 1024 ]] ; then
  LOWPORT="$TOR_OR_PORT"
elif [[ $TOR_OBFS_PORT -le 1024 ]] ; then
  LOWPORT="$TOR_OBFS_PORT"
fi
if [[ ! -z "$LOWPORT" ]] ; then 
  echo Be sure to be able to use port ${LOWPORT}!
  echo
fi

echo Saved your new docker-compose.yml!
}
############################
#      END of main
############################


setNewVariables(){
  read -p "Nickname: " -r
  TOR_NICK="$REPLY"
  read -p "Your E-Mail: " -r
  TOR_MAIL="$REPLY"
  MY_IP4=$(curl ifconfig.me -s)
  echo Your IPv4: "$MY_IP4"
  MY_IP6=$(dig @resolver1.opendns.com AAAA myip.opendns.com +short)
  [[ -z "$MY_IP6" ]] || echo Your IPv6: "$MY_IP6"
  read -p "OnionRouting Port: " -r
  TOR_OR_PORT="$REPLY"
  TOR_OBFS4_PORT=9050
  read -p "Expose obfs4 at...? (9050): " -r
  EXPOSE_OBFS_AT="$REPLY"
  [[ -z "$EXPOSE_OBFS_AT" ]] && EXPOSE_OBFS_AT=$TOR_OBFS4_PORT 
  echo
  read -p "Limit bandwith to (100KBybtes): " -r
  TOR_BANDWITH_RATE=$([[ -z "$REPLY" ]] && echo '100KBytes' || echo $REPLY)
  read -p "Temporarily allow bursts to (300KBytes): "
  TOR_BANDWITH_BURST=$([[ -z "$REPLY" ]] && echo '300KBytes' || echo $REPLY)
  read -p "Enable max accounting per month? [Y/n] " -n 1 -r
  if [[ ! $REPLY =~ ^[Nn]$ ]] ; then
    read -p "(250GBytes) "
    TOR_MAX_ACCOUNTING=$([[ -z "$REPLY" ]] && echo '250GBytes' || echo $REPLY)
  fi

  cat <<EOF > .env
TAG=v1.5

TOR_NICK=$TOR_NICK
TOR_MAIL=$TOR_MAIL

MY_IP4=$MY_IP4
MY_IP6=$MY_IP6

TOR_OR_PORT=$TOR_OR_PORT
TOR_OBFS4_PORT=$TOR_OBFS4_PORT
# Useful if you use a proxy. Else set it to the same value as TOR_OBFS4_PORT
EXPOSE_OBFS_AT=$EXPOSE_OBFS_AT

# Accounting information. Value per month. Leave MAX_ACCOUNTING empty to disable accounting
# (For help, look here: https://www.torproject.org/docs/tor-doc-relay)
TOR_MAX_ACCOUNTING=$TOR_MAX_ACCOUNTING
TOR_BANDWITH_RATE=$TOR_BANDWITH_RATE
TOR_BANDWITH_BURST=$TOR_BANDWITH_BURST
EOF

}

substitute(){
  cp docker-compose.yml.template _tmp.yml
  sed -i 's/:\-[a-zA-Z0-9]*\}/\}/g' _tmp.yml
  sed -i '/^### .*$/d' _tmp.yml
  sed -i "s/expose 443/expose ${EXPOSE_OBFS_AT}/g" _tmp.yml
  envsubst < _tmp.yml > docker-compose.yml
  bindVolumes
  rm _tmp.yml
  setProxy
}

findProxyNetwork(){
  if [[ $EUID -eq 0 || ! -z "$(groups | grep 'docker')" ]]; then
    PROXY_NETWORK="$(docker network ls --format {{.Name}} | grep proxy)"
  fi

  if [[ ! -z "$PROXY_NETWORK" ]]; then
    echo -e -n "Is \033[1m${PROXY_NETWORK}\033[0m the right network? [Y/n] "
    read -n 1 -r
  fi

  if [[ -z "$PROXY_NETWORK" || $REPLY =~ ^[Nn]$ ]]; then
    echo
    read -p "Tell me the complete network name: " -r
    PROXY_NETWORK="$REPLY"
  fi
}

setProxy(){
  read -p "Are you using a proxy? [Y/n] " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Nn]$ ]]; then
    sed -i '/^ .*VIRTUAL_HOST.*$/d' docker-compose.yml
    sed -i '/^.*network.*$/d' docker-compose.yml
    sed -i '/^.*external:.*$/d' docker-compose.yml
  else
    findProxyNetwork
    sed -i 's/ *# * \- "VIRTUAL\_HOST/      \- "VIRTUAL\_HOST/g' docker-compose.yml
    sed -i 's/#networks:/networks:/g' docker-compose.yml
    sed -i "s/^ *# *proxy_network:$/  ${PROXY_NETWORK}:/g" docker-compose.yml
    sed -i "s/^ *# *\- proxy_network$/     \- ${PROXY_NETWORK}/g" docker-compose.yml
    sed -i 's/^# *external.*/    external: true/g' docker-compose.yml
  fi
  echo
}

bindVolumes(){
  if [[ -d ./keys ]] ; then
    sed -i 's/   - \.\/keys/#   - \.\/keys/g' docker-compose.yml
  fi
}

main "$@"
