#! /bin/bash

main() {
  if [[ -d ./keys ]]; then
    echo Did you already export your keys?
    echo If you want to do so once more, please remove or rename the folder ´keys´
    exit 0
  fi

  if [[ $(id -u) -ne 0 ]]; then
    echo This script must be run as root!
    echo Try sudo $0
    exit 1
  fi

  echo ====== Exporting your keys to ./keys ======
  exportKeys \
    && echo Done
# Uncomment if you want to use a local directoy insted of a docker volume
#    && rewriteComposeFile \  
  exit 0
}

exportKeys() {
  CONTAINER=$(docker ps --format {{.Names}} | grep obfs4)
  isUp "$CONTAINER"
  while [[ "$STATE" != 'UP' ]]; do
    read -p 'Could not determine the right container. Is it running? [Y/n] ' -r -n 1
    echo
    if [[ $REPLY =~ ^[Nn]$ ]]; then
      echo Exiting
      exit 1
    else
      read -p "  Please tell me the whole name: " -r CONTAINER
      isUp "$CONTAINER"
      echo
    fi
  done

  TOR_DIR=$(docker exec ${CONTAINER} sh -c 'echo ${TOR_DIR}')

  mkdir ./keys
  docker cp ${CONTAINER}:${TOR_DIR}/keys/secret_id_key ./keys/
  docker cp ${CONTAINER}:${TOR_DIR}/keys/ed25519_master_id_secret_key ./keys/
  docker cp ${CONTAINER}:${TOR_DIR}/keys/ed25519_master_id_public_key ./keys/
  chown -R 100:65533 ./keys
  chmod -R 600 ./keys
}

# Not used at the moment
rewriteComposeFile() {
  if sed -i 's/#   \- obfs4\_bridge\_keys/   \- \.\/keys/g' docker-compose.yml ; then
    sed -i '/^volumes:$/d' docker-compose.yml
    sed -i '/^  obfs4\_bridge\_keys:$/d' docker-compose.yml
    echo 'Bound ./keys directory in docker-compose.yml'
  else
    echo You need to bind ´./keys´ to your new container as ´${TOR_DIR}/keys/´
  fi
}

isUp() {
  STATE=$( [[ $(docker inspect $1 --format {{.State}} 2>/dev/null) =~ ^\{running.*$ ]] && echo UP || echo DOWN)
}

# Executing here
 main 
