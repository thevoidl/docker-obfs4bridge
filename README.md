## Dockerized obfs4 bridge for the tor network
still work in progess...

 * [**What is a Tor Bridge?**](https://support.torproject.org/censorship/censorship-7/)

A bridge provides access to the tor network for people in cencored contries.

 * [**What is obfs4?**](https://gitlab.com/yawning/obfs4/-/blob/master/doc/obfs4-spec.txt)

A protocol that scrambles the traffic to not look like tor. That makes it harder to restrict access to tor.


# This is a lightweight docker image that focuses on two aspects:
 * **Being available behind a proxy.** So any webserver out there is capable of being a tor relay/bridge.
 * **Always being up to date** This container implements a live update script.


## Features:
 * **Installation helper!**
  If you're new to either Docker or Tor, you can use my helper script: See below
 * **Live updates:**
  run `docker exec --user root tor_obfs4-bridge update.sh` and wait a minute...
 * **Run behind a proxy** (not fully tested)
  One of the mail goals for this images is to run behind a proxy! See below
 * **Lightweight:**
  builds on *Linux Alpine* and I tried to use as little dependencies as possible: only *git* and *go* to enable the live updates.
 * **IPv6** (Planned)

## Installation
You will need [docker](https://linuxize.com/post/how-to-install-and-use-docker-on-ubuntu-20-04/) and docker-compose.
```
1. Clone this repository: `git clone https://gitlab.com/thevoidl/docker-obfs4bridge.git tor` (Into the directory tor)
2. cd into the cloned directory: `cd tor`
3. Edit the `.env` to your purposes and copy `docker-compose.yml.template` to `docker-compose.yml`
    **OR**
   use the script `make-compose-file.sh`.
4. Start the container (and build it if it's the first time): `docker-compose up -d`
5. View the logs any time you want: `docker logs tor_obfs4-bridge -f`
6. Backup the keys: `sudo ./export-keys`.
```

## Helper scripts:
 * **Initial Setup**
When you want to have a more beautiful docker-compose file or don't really know how to use the `.env` file:
while you're in the project directory run `./make-compose-file.sh` and the script will led you throug the initial steps.
After that, inspect the generated file once more please.

 * **Bridge Line**
To share your gate with friends you'll need the so called **bridge line**. Get it with:
`docker exec tor_obfs4-bridge get-bridge-line.sh`
But be carful with this line! It shouldn't become public!

 * **Update**
`docker exec --user root tor_obfs4-bridge update.sh`. This will only apply on a running container. For advantage that can be run as cron-job.
A once stopped container will reboot in the state it was build. To rebuild the image with up-to-date tor software type `docker-compose build`.

The private keys will be stored in a docker volume. That allows shutting down or rebuilding the container without losing the Tor identity.

 * **Move container or switch to another software**
Note that this image uses docker volumes to store the keys. To use another software than this image with the same tor ID you will have to export the private keys. Run `sudo ./export-keys.sh`. This script will export the neccessare keys from the container to a folder named *keys*.

 * **View Stats**
To look up your stats at [**Relay Search**](https://metrics.torproject.org):
`docker exec tor_obfs4-bridge relay-search.sh`

 * **Quick overview/test**
`docker exec tor_obfs4-bridge test.sh`

## Setup with proxy
 * jwilder/ginx-proxy
Most certainly you will have the docker-compose file for the proxy at another location than this project. It will route the requests for your site to the right micro service depending on the domain. On the proxy-site there is nothing to do for this bridge to work!
This proxy uses the environment variables `VIRTUAL_HOST`. Just set it to your IP:PORT for the bridge. E.g. `VIRTUAL_HOST=10.9.8.7:443`
The second thing to do is exposing the ports. Map them to the the default ports of the container:
```
  ports:
    - 9050:9050
    - 443:9010
```

# Thanks to:
* [Philip Winter, The Tor Project](https://github.com/NullHypothesis/docker-obfs4-bridge) - Official OBFS4 bridge based on Debian
* [Elfrinjo](https://github.com/elfrinjo/dockerfiles/tree/master/torrelay) - Dockerized Tor Relay
* [Yawning](https://gitlab.com/yawning/obfs4) - Obfs4
